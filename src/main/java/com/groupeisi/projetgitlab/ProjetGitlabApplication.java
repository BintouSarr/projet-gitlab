package com.groupeisi.projetgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetGitlabApplication.class, args);
	}

}
