FROM openjdk:17-jdk-slim

EXPOSE 9091

ADD target/projet-gitlab-0.0.1-SNAPSHOT.jar projet-gitlab.jar

CMD ["java", "-jar", "projet-gitlab.jar"]
